# AAPAFile - Auto Angular Public API File
A tool to generate the public-api.ts file that is used in angular libraries for all typescript files in the library project directory.
The tool just generates simple "export * from" lines.

## Dependencies
This tool uses minimal dependencies: CommandLine for setting up a command line application.
See the Thirdpartylicenses folder for more information