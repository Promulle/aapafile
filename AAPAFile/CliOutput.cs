using System;

namespace AAPAFile
{
  public class CliOutput
  {
    private readonly bool m_Verbose;
    
    public CliOutput(bool i_Verbose)
    {
      m_Verbose = i_Verbose;
    }
    
    private static void OutputColored(string i_Value, ConsoleColor? i_Color)
    {
      if (i_Color != null)
      {
        var prevColor = Console.ForegroundColor;
        Console.ForegroundColor = i_Color.Value;
        Console.WriteLine(i_Value);
        Console.ForegroundColor = prevColor;
      }
      else
      {
        Console.WriteLine(i_Value);
      }
    }
    
    public void OutputAlways(string i_Value)
    {
      Output(i_Value, false);
    }

    public void OutputVerbose(string i_Value)
    {
      Output(i_Value, true);
    }

    public void OutputError(string i_ErrorMessage)
    {
      OutputColored(i_ErrorMessage, ConsoleColor.Red);
    }

    public void OutputWarning(string i_WarningMessage)
    {
      OutputColored(i_WarningMessage, ConsoleColor.Yellow);
    }

    public void OutputSuccess(string i_SuccessMessage)
    {
      OutputColored(i_SuccessMessage, ConsoleColor.Green);
    }

    public void Output(string i_Value, bool i_OnlyOnVerbose)
    {
      if (!i_OnlyOnVerbose || m_Verbose)
      {
        OutputColored(i_Value, null);
      }
    }
  }
}