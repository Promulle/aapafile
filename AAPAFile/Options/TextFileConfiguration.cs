using System;
using System.Text;

namespace AAPAFile.Options
{
  public class TextFileConfiguration
  {
    public string NewLine { get; init; }
    
    public Encoding Encoding { get; init; }
    
    public char QuoteCharacter { get; init; }

    public static TextFileConfiguration From(CliOptions i_Options, CliOutput i_Output)
    {
      var encoding = DetermineEncoding(i_Options, i_Output);
      if (encoding == null)
      {
        throw new InvalidOperationException("No encoding. Not continuing with text generation");
      }
      var newLine = DetermineNewLine(i_Options);
      var stringQuoteCharacter = DetermineQuotationCharacter(i_Options);
      return new TextFileConfiguration()
      {
        Encoding = encoding,
        NewLine = newLine,
        QuoteCharacter = stringQuoteCharacter
      };
    }
    
    private static char DetermineQuotationCharacter(CliOptions i_Options)
    {
      return i_Options.QuoteMode switch
      {
        StringQuoteMode.Single => '\'',
        StringQuoteMode.Double => '\"',
        _ => '\''
      };
    }

    private static string DetermineNewLine(CliOptions i_Options)
    {
      return i_Options.NewLineMode switch
      {
        NewLineMode.Native => Environment.NewLine,
        NewLineMode.CRLF => "\\r\\n",
        NewLineMode.LF => "\\n",
        _ => Environment.NewLine
      };
    }

    private static Encoding DetermineEncoding(CliOptions i_Options, CliOutput i_Output)
    {
      if (i_Options.EncodingCodePage != null)
      {
        try
        {
          i_Output.OutputVerbose($"Getting encoding for codepage {i_Options.EncodingCodePage.Value}");
          var encoding = Encoding.GetEncoding(i_Options.EncodingCodePage.Value);
          if (encoding == null)
          {
            throw new InvalidOperationException("Encoding was null.");
          }
          i_Output.OutputVerbose(encoding.ToString());
          return encoding;
        }
        catch (Exception ex)
        {
          i_Output.OutputError($"Could not determine encoding for codepage {i_Options.EncodingCodePage.Value}. Exception: {ex}");
          return null;
        }
      }
      if (i_Options.Encoding != null)
      {
        return i_Options.Encoding.Value switch
        {
          DefaultEncodings.UTF8 => Encoding.UTF8,
          DefaultEncodings.ASCII => Encoding.ASCII,
          DefaultEncodings.UTF32 => Encoding.UTF32,
          DefaultEncodings.Latin1 => Encoding.Latin1,
          DefaultEncodings.Unicode => Encoding.Unicode,
          DefaultEncodings.BigEndianUnicode => Encoding.BigEndianUnicode,
          _ => Encoding.UTF8
        };
      }
      
      return Encoding.UTF8;
    }
  }
}