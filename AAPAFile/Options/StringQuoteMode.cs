namespace AAPAFile.Options
{
  public enum StringQuoteMode
  {
    Single = 0,
    Double = 1
  }
}