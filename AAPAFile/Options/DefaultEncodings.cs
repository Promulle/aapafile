namespace AAPAFile.Options
{
  public enum DefaultEncodings
  {
    UTF8 = 0,
    ASCII = 1,
    UTF32 = 3,
    Latin1 = 4,
    Unicode = 5,
    BigEndianUnicode = 6
  }
}