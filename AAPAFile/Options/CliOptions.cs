using System.Collections.Generic;
using CommandLine;

namespace AAPAFile.Options
{
  public class CliOptions
  {
    [Option('d', "directory", Required = true, HelpText = "Directory to work in. Mostlikely the project root (src directory) of the library you want to generate a public-api.ts file for.")]
    public string WorkingDirectory { get; set; }
    
    [Option('v', "verbose", Required = false, Default = false, HelpText = "If Enabled, the tool will output more information about what it is actually doing.")]
    public bool Verbose { get; set; }
    
    [Option('f', "fileName", Required = false, Default = "public-api.ts", HelpText = "Name of file to output the public api to. This will be combined with -d/--directory to a full path for the file.")]
    public string OutputFileName { get; set; }
    
    [Option('q', "quoteMode", Required = false, Default = StringQuoteMode.Single, HelpText = "Mode of string quotation to use. 'content' or \"content\". Modes: 'Single' or 'Double'")]
    public StringQuoteMode QuoteMode { get; set; }
    
    [Option('l', "newLineMode", Required = false, Default = NewLineMode.Native, HelpText = "Determines what kind of newline character to use: Native: just uses Environment.NewLine, CRLF: \\r\\n, LF: \\n")]
    public NewLineMode NewLineMode { get; set; }
    
    [Option('e', "encoding", Required = false, Default = DefaultEncodings.UTF8, HelpText = "Encoding to use when writing to the file. This is an enum that corresponds to the default encodings available in the C# Encoding static class.")]
    public DefaultEncodings? Encoding { get; set; }
    
    [Option('c', "encodingCodePage", Required = false, Default = null, HelpText = "Encoding codepage number to use for encoding. If this option is set, it is used instead of the Encoding option")]
    public int? EncodingCodePage { get; set; }
    
    [Option("prefixLine", Required = false, HelpText = "Line that should be added before any imports. Multiple ok")]
    // ReSharper disable once CollectionNeverUpdated.Global Updated from library
    public IEnumerable<string> PrefixLines { get; set; }
  }
}