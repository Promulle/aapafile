namespace AAPAFile.Options
{
  public enum NewLineMode
  {
    Native = 0,
    CRLF = 1,
    LF = 2
  }
}