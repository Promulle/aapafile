﻿using System;
using AAPAFile;
using AAPAFile.Options;
using CommandLine;

Console.WriteLine("Auto Angular Public-api.ts File v1.0");
Parser.Default.ParseArguments<CliOptions>(args)
  .WithParsed(CliExecution.Execute);