using System;
using System.IO;
using System.Linq;
using System.Text;
using AAPAFile.Options;
using AAPAFile.Extensions;

namespace AAPAFile.TextGeneration
{
  public static class PublicApiText
  {
    public static PublicApiTextResult ForFiles(CliOptions i_Options, CliOutput i_Output)
    {
      var conf = TextFileConfiguration.From(i_Options, i_Output);
      var projName = new DirectoryInfo(i_Options.WorkingDirectory).Parent?.Name;
      var libPath = Path.Join(i_Options.WorkingDirectory, "lib");
      if (!Directory.Exists(libPath))
      {
        throw new InvalidOperationException($"No such directory: {libPath}");
      }
      return new PublicApiTextResult()
      {
        Configuration = conf,
        Text = GetTextForFile(i_Options, projName, libPath, conf, i_Output)
      };
    }

    private static string GetTextForFile(CliOptions i_Options, string i_ProjName, string i_LibPath, TextFileConfiguration i_Conf, CliOutput i_Output)
    {
      var builder = new StringBuilder();
      builder.AppendConfiguredLine("/*", i_Conf)
        .Append(" * Public API Surface of ")
        .AppendConfiguredLine(i_ProjName, i_Conf)
        .AppendConfiguredLine(" */", i_Conf);
      var prefixLines = i_Options.PrefixLines?.ToArray() ?? Array.Empty<string>();
      if (prefixLines.Length > 0)
      {
        builder.AppendLine("//Prefix Lines");
        foreach (var prefixLine in prefixLines)
        {
          builder.AppendLine(prefixLine);
        }
      }
      foreach (var file in Directory.GetFiles(i_LibPath, "*.ts").OrderBy(r => r))
      {
        var exportLine = GetExportLine(i_Options.WorkingDirectory, file, i_Conf);
        builder.AppendConfiguredLine(exportLine, i_Conf);
      }
      foreach (var directory in Directory.GetDirectories(i_LibPath).OrderBy(r => r))
      {
         AddFilesToBuilder(i_Options.WorkingDirectory, directory, i_LibPath, builder, i_Conf, i_Output);
      }
      return builder.ToString();
    }

    private static void AddFilesToBuilder(string i_WorkingDirectory, string i_Directory, string i_LibPath, StringBuilder t_Builder, TextFileConfiguration i_Conf, CliOutput i_Output)
    {
      var dirName = EnsurePathWithCorrectCharacter(i_Directory.Replace(i_LibPath, ""));
      var regionLine = $"//#region {dirName}";
      i_Output.OutputVerbose(regionLine);
      t_Builder.AppendConfiguredLine(i_Conf)
        .AppendConfiguredLine(regionLine, i_Conf);
      foreach (var directory in Directory.GetDirectories(i_Directory).OrderBy(r => r))
      {
        i_Output.OutputVerbose($"Processing directory: {directory}");
        AddFilesToBuilder(i_WorkingDirectory, directory, i_LibPath, t_Builder, i_Conf, i_Output);
      }
      foreach (var file in Directory.GetFiles(i_Directory, "*.ts").OrderBy(r => r))
      {
        i_Output.OutputVerbose($"Processing file: {file}");
        var exportLine = GetExportLine(i_WorkingDirectory, file, i_Conf);
        i_Output.OutputVerbose(exportLine);
        t_Builder.AppendConfiguredLine(exportLine, i_Conf);
      }
      var endRegionLine = $"//#endregion {dirName}";
      i_Output.OutputVerbose(endRegionLine);
      t_Builder.AppendConfiguredLine(endRegionLine, i_Conf);
    }

    private static string GetExportLine(string i_WorkingDirectory, string i_FilePath, TextFileConfiguration i_Configuration)
    {
      var replacedPath = i_FilePath.Replace(i_WorkingDirectory, ".");
      var fileName = Path.GetFileNameWithoutExtension(replacedPath);
      var dir = Path.GetDirectoryName(replacedPath);
      var filePath = Path.Join(dir, fileName);
      var defaultLine = $"export * from {i_Configuration.QuoteCharacter}{filePath}{i_Configuration.QuoteCharacter};";
      return EnsurePathWithCorrectCharacter(defaultLine);
    }

    private static string EnsurePathWithCorrectCharacter(string i_OriginalPath)
    {
      if (OperatingSystem.IsWindows() && i_OriginalPath.Contains('\\'))
      {
        return i_OriginalPath.Replace("\\", "/");
      }
      return i_OriginalPath;
    }
  }
}