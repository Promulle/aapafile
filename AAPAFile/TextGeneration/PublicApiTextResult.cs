using AAPAFile.Options;

namespace AAPAFile.TextGeneration
{
  public class PublicApiTextResult
  {
    /// <summary>
    /// Text to be written to the file
    /// </summary>
    public string Text { get; set; }
    
    /// <summary>
    /// Configuration the text was created with
    /// </summary>
    public TextFileConfiguration Configuration { get; set; }
  }
}