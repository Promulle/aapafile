using System.Text;
using AAPAFile.Options;

namespace AAPAFile.Extensions
{
  public static class StringBuilderExtensions
  {
    public static StringBuilder AppendConfiguredLine(this StringBuilder i_Builder, string i_Value, TextFileConfiguration i_Configuration)
    {
      return i_Builder.Append(i_Value).Append(i_Configuration.NewLine);
    }
    
    public static StringBuilder AppendConfiguredLine(this StringBuilder i_Builder, TextFileConfiguration i_Configuration)
    {
      return i_Builder.Append(i_Configuration.NewLine);
    }
  }
}