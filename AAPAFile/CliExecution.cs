using System.IO;
using AAPAFile.Options;
using AAPAFile.TextGeneration;

namespace AAPAFile
{
  public static class CliExecution
  {
    public static void Execute(CliOptions i_Options)
    {
      var consoleOutput = new CliOutput(i_Options.Verbose);
      if (string.IsNullOrEmpty(i_Options.WorkingDirectory))
      {
        consoleOutput.OutputError($"Option: {nameof(i_Options.WorkingDirectory)} was passed as null or empty.");
        return;
      }
      if (!Directory.Exists(i_Options.WorkingDirectory))
      {
        consoleOutput.OutputError($"Option: {nameof(i_Options.WorkingDirectory)} points to a not-existing directory ({i_Options.WorkingDirectory}). Did you specify the correct directory?"); 
      }
      var textResult = PublicApiText.ForFiles(i_Options, consoleOutput);
      var file = Path.Join(i_Options.WorkingDirectory, i_Options.OutputFileName);
      var actualPath = Path.GetFullPath(file);
      consoleOutput.OutputAlways($"Writing text to {actualPath}");
      if (File.Exists(actualPath))
      {
        File.Delete(actualPath);
      }
      File.WriteAllText(actualPath, textResult.Text, textResult.Configuration.Encoding);
      consoleOutput.OutputSuccess("Done writing to file! API File successfully created");
    }
  }
}